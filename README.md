Provisioning script for a Windows 10 development machine
=====

# Manual steps before

These steps must be done before running the Ansible playbook

* Use the [Windows 10 media creation tool](https://www.microsoft.com/en-gb/software-download/windows10) to create a USB
* Install the windows using USB
* Enable WSL `Enable-WindowsOptionalFeature -FeatureName "Microsoft-Windows-Subsystem-Linux" -Online -NoRestart:$False ;`
* Download Ubuntu `Invoke-WebRequest -Uri https://aka.ms/wsl-ubuntu-1804 -OutFile Ubuntu.appx -UseBasicParsing ;`
* Install Ubuntu `./Ubuntu.appx ;`
* Enable Windows Remote Management `iwr https://raw.githubusercontent.com/ansible/ansible/devel/examples/scripts/ConfigureRemotingForAnsible.ps1 -UseBasicParsing | iex`
* Open the Ubuntu terminal
* Add Ansible repository `sudo apt-add-repository ppa:ansible/ansible`
* Update package list `sudo apt-get update`
* Install Ansible `sudo apt-get install ansible`
* Install Pip for Python `sudo apt-get install python-pip`
* Install WinRM for Python `sudo pip install pywinrm`

# Running

This script is meant to be run inside the machine you want to provision. 

It is expected that:

* the machine has a Linux installed using Windows Linux Subsystem
* the machine has Ansible installed in the Linux environment
* the machine has WinRM configured for Ansible access

Steps to provision:

* Change the username and password in the `inventory.yml` file
* Run the Ansible playbook inside the Ubuntu `ansible-playbook -i inventory.yml main.yml`

# Manual steps after

These steps must be done after running the Ansible playbook
